import os
import numpy as np
import xml.etree.ElementTree as ET
import yaml
import copy

# List *.xml
_DATA_DIR = 'D:/WORKSPACES/QuadrepProjects/15metal-picking/bin/records_20210312'
paths = []
for file in os.listdir(_DATA_DIR):
    if file.endswith('.xml'):
        paths.append(_DATA_DIR + '/' + file)

# TODO create set of angles
step = 10
angle_categories = []
angle_set = []
for angle in range(-170, 190, 10):
    angle_categories.append(angle)
    angles = list(range(angle - step // 2, angle + step // 2, 1))
    angles = [value if value >=-180 else 360 + value for value in angles]
    angles = [value if value <=180 else -360 + value for value in angles]
    angle_set.append(angles)
print(angle_categories, '\n', angle_set)

def find_belonged_set(value, list_of_set):
    for i, set in enumerate(list_of_set):
        if value in set:
            return i
    return None

# For each file xml:
for path in paths:
    ## Load & parse xml
    tree = ET.parse(path)
    # new_tree = copy.deepcopy(tree)
    root = tree.getroot()
    filename = os.path.splitext(root.find('filename').text)[0];
    ## Obtain resolution: w-h
    size = root.find('size')
    width, height = int(size.find('width').text), int(size.find('height').text);
    print(filename, width, height)

    ## For each obtained object:
    for obj in root.findall('object'):
        ### Obtain class name -> infer class index
        category = obj.find('name').text; print(category)

        ### Obtain XYmin-max -> infer Centerxy & Sizexy
        bndbox = obj.find('bndbox')
        xmin, ymin, xmax, ymax = int(bndbox.find('xmin').text), int(bndbox.find('ymin').text), int(bndbox.find('xmax').text), int(bndbox.find('ymax').text); print(xmin, ymin, xmax, ymax)
        # TODO measure A, B, angle(AB, Ox), check which set angle belonged to
        if category == 'DR':
            A = [xmin, ymin]
            B = [xmax, ymax]
        elif category == 'DL':
            A = [xmax, ymin]
            B = [xmin, ymax]
        elif category == 'UL':
            A = [xmax, ymax]
            B = [xmin, ymin]
        elif category == 'UR':
            A = [xmin, ymax]
            B = [xmax, ymin]
        else:
            continue
        vectAB = [A[0] - B[0], A[1] - B[1]]
        angle = int(np.arctan2(vectAB[1], vectAB[0]) / np.pi * 180)
        angle_cate_ind = find_belonged_set(angle, angle_set)
        ### modify xmaxmin/ymaxmin if size is too small
        if xmax - xmin < 10:
            xmin_n, xmax_n = xmin - 5, xmax + 5
        else:
            xmin_n, xmax_n = xmin, xmax
        if ymax - ymin < 10:
            ymin_n, ymax_n = ymin - 5, ymax + 5
        else:
            ymin_n, ymax_n = ymin, ymax
        ### Modify data
        obj.find('name').text = f'{angle_categories[angle_cate_ind]}'
        bndbox.find('xmin').text = f'{xmin_n}'
        bndbox.find('xmax').text = f'{xmax_n}'
        bndbox.find('ymin').text = f'{ymin_n}'
        bndbox.find('ymax').text = f'{ymax_n}'
        ### Insert additional data
        g = ET.SubElement(bndbox, 'xA')
        g.text = f'{A[0]}'
        g = ET.SubElement(bndbox, 'yA')
        g.text = f'{A[1]}'
        g = ET.SubElement(bndbox, 'xB')
        g.text = f'{B[0]}'
        g = ET.SubElement(bndbox, 'yB')
        g.text = f'{B[1]}'
    ## Re-Save xml
    with open(f'{_DATA_DIR}/labels/{filename}.xml', 'wb') as f:
        tree.write(f)