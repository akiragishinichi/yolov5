import os
import numpy as np
import xml.etree.ElementTree as ET
import yaml

# V List *.xml
_DATA_DIR = 'D:/WORKSPACES/QuadrepProjects/14pedal-picking/bin/pedal_high-density_structured_210224/additional_210324/origin_labels'
paths = []
for file in os.listdir(_DATA_DIR):
    if file.endswith('.xml'):
        paths.append(_DATA_DIR + '/' + file)
# print(paths) # ! Delete after test
# num_data = len(paths)
# V Seed & Shuffle list
# np.random.seed(0)
# indices = np.arange(num_data)
# np.random.shuffle(indices)
# print(indices) # ! Delete after test
# V Distribute train/val/test 0.7/0.2/0.1
# indices_train, indices_val, indices_test = indices[:int(num_data * 0.7)], indices[int(num_data * 0.7):int(num_data * 0.9)], indices[int(num_data * 0.9):]
# print(f'{indices_train} \n{indices_val} \n{indices_test}')
# V Generate data yaml
# Read data yaml
_DATA_YAML_PATH = 'D:/WORKSPACES/QuadrepProjects/15metal-picking/thirdparty/yolov5/data/pedal-picking.yaml'
with open(_DATA_YAML_PATH, 'r') as stream:
    data = yaml.safe_load(stream)
    print(data)
    num_category = data['nc']; print(num_category)
    category_names = data['names']; print(category_names)
    assert len(category_names) == num_category, 'List class is not correct'

    # For each file xml:
    for path in paths:
        ## Load & parse xml
        tree = ET.parse(path)
        root = tree.getroot()
        filename = os.path.splitext(root.find('filename').text)[0]; print(filename)
        ## Obtain resolution: w-h
        size = root.find('size')
        width, height = int(size.find('width').text), int(size.find('height').text); print(width, height)

        ## Create new label file(txt format)
        # filename = os.path.splitext(os.path.split(path)[1])[0]
        txt_label_path = f'{_DATA_DIR}/{filename}.txt'
        if os.path.isfile(txt_label_path): # Remove existed label-file
            os.remove(txt_label_path)
        ## For each obtained object:
        for obj in root.findall('object'):
            ### Obtain class name -> infer class index
            category = obj.find('name').text; print(category)
            ### Infer cate index
            if not category in category_names:
                continue
            category_index = category_names.index(category)

            ### Obtain XYmin-max -> infer Centerxy & Sizexy
            bndbox = obj.find('bndbox')
            xmin, ymin, xmax, ymax = int(bndbox.find('xmin').text), int(bndbox.find('ymin').text), int(bndbox.find('xmax').text), int(bndbox.find('ymax').text); print(xmin, ymin, xmax, ymax)
            center_x, center_y = (xmax + xmin) // 2, (ymax + ymin) // 2; print(center_x, center_y)
            size_w, size_h = xmax - xmin, ymax - ymin; print(size_w, size_h)
            center_x_f, center_y_f, size_w_f, size_h_f = center_x / width, center_y / height, size_w / width, size_h / height; print(center_x_f, center_y_f, size_w_f, size_h_f)
            ### Save txt
            with open(txt_label_path, 'a') as file:
                file.write(f'{category_index} {center_x_f} {center_y_f} {size_w_f} {size_h_f}\n')