import os
import numpy as np
from PIL import Image

# V List *.xml
_DATA_DIR = 'D:/WORKSPACES/QuadrepProjects/14pedal-picking/bin/pedal_high-density_structured_210224/additional_210324/origin_images'
_SAVE_DIR = 'D:/WORKSPACES/QuadrepProjects/14pedal-picking/bin/pedal_high-density_structured_210224/additional_210324/images'
paths = []
for file in os.listdir(_DATA_DIR):
    if file.endswith('.png'):
        paths.append(_DATA_DIR + '/' + file)
print(paths)

for path in paths:
    im = Image.open(path)
    # print(im.size)
    im.thumbnail((640, 640), Image.ANTIALIAS)
    # print(im.size)
    filename = os.path.splitext(os.path.split(path)[1])[0]; print(f'{_SAVE_DIR}/{filename}')
    im.save(f'{_SAVE_DIR}/{filename}.png', 'PNG')